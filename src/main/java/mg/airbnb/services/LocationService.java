package mg.airbnb.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mg.airbnb.model.Location;
import mg.airbnb.model.Logement;
import mg.airbnb.model.Utilisateur;
import mg.airbnb.repository.LocationRepository;

@Service
public class LocationService {

	@Autowired
	private UtilisateurService utilisateurService;

	@Autowired
	private LocationRepository locationRepository;

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Location findById(Long id) {
		Location l = null;
		for (Location location : locationRepository.findAll()) {
			if (location.getId() == id) {
				l = location;
				break;
			}
		}
		return l;
	}
}
