package mg.airbnb.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mg.airbnb.model.Location;
import mg.airbnb.model.Logement;
import mg.airbnb.model.Utilisateur;
import mg.airbnb.repository.LogementRepository;

@Service
public class LogementService {

	@Autowired
	private LogementRepository logementRepository;

	@Autowired
	private UtilisateurService utilisateurService;

	/**
	 * 
	 * @return
	 */
	public Boolean isAvailable(Logement logement) {
		return logement.getNbLocataire() > 0;
	}

	/**
	 * 
	 * @return
	 */
	public int countNotifications() {
		int notifs = 0;
		for (Location location : utilisateurService.getLoggedUser().getLocations()) {
			if (location.getValider() == false) {
				notifs++;
			}
		}
		return notifs;
	}

	/**
	 * 
	 * @return
	 */
	public Set<Logement> findAllAvailable(Utilisateur utilisateur) {
		Set<Logement> availables = new HashSet<Logement>();
		for (Logement logement : logementRepository.findAll()) {
			if (this.isAvailable(logement) && !logement.getProprietaire().getSignals().contains(utilisateur)) {
				availables.add(logement);
			}
		}
		return availables;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Utilisateur proprietaire(Long id) {
		Logement logement = this.findById(id);
		return logement.getProprietaire();
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Logement findById(Long id) {
		Logement logement = null;
		for (Logement l : logementRepository.findAll()) {
			if (l.getId() == id) {
				logement = l;
				break;
			}
		}
		return logement;
	}

}
