package mg.airbnb.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import mg.airbnb.model.Utilisateur;
import mg.airbnb.repository.UtilisateurRepository;

@Service
public class UtilisateurService {
	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Autowired
	BCryptPasswordEncoder bcrypt;

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Utilisateur findById(Long id) {
		Utilisateur utilisateur = null;
		for (Utilisateur u : utilisateurRepository.findAll()) {
			if (u.getId() == id) {
				utilisateur = u;
				break;
			}
		}
		return utilisateur;
	}

	/**
	 * Find user by email
	 * 
	 * @param mail
	 * @return
	 */
	public Utilisateur findByMail(String mail) {
		Utilisateur utilisateur = null;
		for (Utilisateur user : utilisateurRepository.findAll()) {
			if (user.getEmail().equalsIgnoreCase(mail)) {
				utilisateur = user;
				break;
			}
		}
		return utilisateur;
	}

	/**
	 * Retrieve logged user
	 * 
	 * @return
	 */
	public Utilisateur getLoggedUser() {
		String username = "";
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken))
			username = auth.getName();
		return findByMail(username);
	}

	/**
	 * Register user
	 * 
	 * @return
	 */
	public Utilisateur save() {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setEmail("zafitina@gmail.com");
		utilisateur.setName("Zafitina");
		utilisateur.setFirstName("Nicolas");
		utilisateur.setPassword(bcrypt.encode(utilisateur.getPassword()));
		return utilisateurRepository.save(utilisateur);
	}
}
