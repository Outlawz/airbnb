package mg.airbnb.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import mg.airbnb.model.Location;

public interface LocationRepository extends CrudRepository<Location, Long> {

	Optional<Location> findById(Long id);

	boolean existsById(Long id);

	Set<Location> findAll();

	long count();

	void deleteById(Long id);

	void delete(Location l);

	void deleteAll();
}
