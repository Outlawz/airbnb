package mg.airbnb.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.sun.xml.bind.v2.model.core.ID;

import mg.airbnb.model.Utilisateur;

public interface UtilisateurRepository extends CrudRepository<Utilisateur, Long> {

	Optional<Utilisateur> findById(Long id);

	boolean existsById(ID id);

	Utilisateur save(Utilisateur utilisateur);

	Set<Utilisateur> findAll();

	long count();

	void deleteById(Long id);

	void delete(Utilisateur u);

	void deleteAll();
}
