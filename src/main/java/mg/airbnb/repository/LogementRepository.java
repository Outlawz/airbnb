package mg.airbnb.repository;

import java.util.Date;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import mg.airbnb.model.Logement;

public interface LogementRepository extends CrudRepository<Logement, Long> {

	Optional<Logement> findById(Long id);

	boolean existsById(Long id);

	Set<Logement> findAll();

	long count();

	void deleteById(Long id);

	void delete(Logement l);

	void deleteAll();

	@Query("SELECT u FROM Logement u WHERE (:ville IS NULL OR u.ville LIKE %:ville%)"
			+ "AND (:debut IS NULL OR u.disponibilite >= :debut)"
			+ "AND (:personnes < 0 OR u.nbLocataire <= :personnes)" + "AND (:prix < 0 OR u.prix <= :prix)")
	Set<Logement> filters(@Param("ville") String ville, @Param("debut") Date debut, @Param("personnes") int personnes,
			@Param("prix") int prix);
}
