package mg.airbnb.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import mg.airbnb.model.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {

	Optional<Role> findById(Long id);

	boolean existsById(Long id);

	Set<Role> findAll();

	long count();

	Role save(Role role);

	void deleteById(Long id);

	void delete(Role l);

	void deleteAll();
}
