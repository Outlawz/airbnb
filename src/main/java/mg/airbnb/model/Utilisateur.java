package mg.airbnb.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "utilisateurs")
public class Utilisateur implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	private String name;
	@NotNull
	private String firstName;
	@NotNull
	private String email;
	@NotNull
	private long accountNumber;
	@NotNull
	private String password;
	@OneToMany(mappedBy = "proprietaire")
	@JsonIgnore
	private Set<Logement> logements;
	@OneToMany(mappedBy = "locataire")
	private Set<Location> locations;
	@OneToMany(mappedBy = "signalBy")
	private Set<Utilisateur> signals;
	@ManyToOne
	private Utilisateur signalBy;
	@ManyToOne
	private Role role;

	public Utilisateur() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String _password) {
		this.password = _password;
	}

	public Set<Logement> getLogements() {
		return logements;
	}

	public void setLogements(Set<Logement> logements) {
		this.logements = logements;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<Location> getLocations() {
		return locations;
	}

	public void setLocations(Set<Location> locations) {
		this.locations = locations;
	}

	public Set<Utilisateur> getSignals() {
		return signals;
	}

	public void setSignals(Set<Utilisateur> signals) {
		this.signals = signals;
	}

	public Utilisateur getSignalBy() {
		return signalBy;
	}

	public void setSignalBy(Utilisateur signalBy) {
		this.signalBy = signalBy;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
}
