package mg.airbnb.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "logements")
public class Logement implements Serializable {

	@Id
	@GeneratedValue
	private Long id;
	private int nbPieces;
	private int superficie;
	private String adresse;
	private String ville;
	private Double prix;
	private int nbLocataire;
	private TypeLogement type;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date disponibilite;
	@ManyToOne
	private Utilisateur proprietaire;
	@OneToMany(mappedBy = "logement")
	private Set<Location> locataires;

	public Logement() {

	}

	/** GETTERS AND SETTERS **/
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNbPieces() {
		return nbPieces;
	}

	public void setNbPieces(int nbPieces) {
		this.nbPieces = nbPieces;
	}

	public int getSuperficie() {
		return superficie;
	}

	public void setSuperficie(int superficie) {
		this.superficie = superficie;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public Double getPrix() {
		return prix;
	}

	public void setPrix(Double prix) {
		this.prix = prix;
	}

	public Utilisateur getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(Utilisateur proprietaire) {
		this.proprietaire = proprietaire;
	}

	public int getNbLocataire() {
		return nbLocataire;
	}

	public void setNbLocataire(int nbLocataire) {
		this.nbLocataire = nbLocataire;
	}

	public TypeLogement getType() {
		return type;
	}

	public void setType(TypeLogement type) {
		this.type = type;
	}

	public Date getDisponibilite() {
		return disponibilite;
	}

	public void setDisponibilite(Date disponibilite) {
		this.disponibilite = disponibilite;
	}

	public Set<Location> getLocataires() {
		return locataires;
	}

	public void setLocataires(Set<Location> locataires) {
		this.locataires = locataires;
	}
}
