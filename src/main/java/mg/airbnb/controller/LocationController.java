package mg.airbnb.controller;

import java.io.IOException;
import java.util.Date;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import mg.airbnb.model.Location;
import mg.airbnb.model.Logement;
import mg.airbnb.model.Utilisateur;
import mg.airbnb.repository.LocationRepository;
import mg.airbnb.services.LocationService;
import mg.airbnb.services.LogementService;
import mg.airbnb.services.UtilisateurService;

@CrossOrigin(origins = "*")
@Controller
public class LocationController {

	@Autowired
	LocationRepository locationRepository;

	@Autowired
	LogementService logementService;

	@Autowired
	LocationService locationService;

	@Autowired
	UtilisateurService utilisateurService;

	private final String url = "/location";

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = url + "s", method = RequestMethod.GET)
	public ModelAndView findAll() {
		ModelAndView mav = new ModelAndView("locations");
		Utilisateur utilisateur = utilisateurService.getLoggedUser();
		mav.addObject("locations", utilisateur.getLocations());
		mav.addObject("utilisateur", utilisateurService.getLoggedUser());
		mav.addObject("notifications", logementService.countNotifications());
		return mav;
	}

	/**
	 * 
	 * @param location
	 * @param result
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = url + ".store/{date}/{id}", method = RequestMethod.POST)
	public String save(@PathVariable Long date, @PathVariable Long id) throws IOException {
		Logement logement = logementService.findById(id);
		Utilisateur utilisateur = utilisateurService.getLoggedUser();
		Location location = new Location();
		location.setDebut(new Date(date));
		location.setValider(false);
		location.setLogement(logement);
		location.setLocataire(utilisateur);
		locationRepository.save(location);
		logement.getLocataires().add(location);
		utilisateur.getLocations().add(location);

		// Envoi requête pour débiter le compte de l'utilisateur
		long locataire = utilisateur.getAccountNumber();
		long amount = logement.getPrix().longValue();
		long proprietaire = logement.getProprietaire().getAccountNumber();
		// Débiter le locataire
		HttpPost post = new HttpPost("http://127.0.0.1:8081/debit/" + amount + "/" + locataire + "/" + proprietaire);

		try (CloseableHttpClient httpClient = HttpClients.createDefault();
				CloseableHttpResponse response = httpClient.execute(post)) {

			String result = EntityUtils.toString(response.getEntity());
			System.err.println(result);
		}
		// Créditer le propriétaire
		HttpPost post2 = new HttpPost("http://127.0.0.1:8081/crédit/" + amount + "/" + proprietaire + "/" + locataire);

		try (CloseableHttpClient httpClient = HttpClients.createDefault();
				CloseableHttpResponse response = httpClient.execute(post2)) {

			String result2 = EntityUtils.toString(response.getEntity());
			System.err.println(result2);
		}
		return "redirect:/home";
	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = url + ".validate/{id}", method = RequestMethod.POST)
	public String validate(@PathVariable Long id) {
		Location location = locationService.findById(id);
		if (location.getLogement().getNbLocataire() >= 1) {
			location.getLogement().setNbLocataire(location.getLogement().getNbLocataire() - 1);
			location.setValider(true);
			locationRepository.save(location);
		}
		return "redirect:/logements";
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = url + ".delete/{id}", method = RequestMethod.POST)
	public String delete(@PathVariable Long id) {
		Location location = locationService.findById(id);
		if (location.getValider() == true) {
			location.getLogement().setNbLocataire(location.getLogement().getNbLocataire() + 1);
		}
		locationRepository.deleteById(id);
		return "redirect:/logements";
	}
}
