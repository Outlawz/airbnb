package mg.airbnb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import mg.airbnb.model.Role;
import mg.airbnb.model.Utilisateur;
import mg.airbnb.repository.RoleRepository;
import mg.airbnb.repository.UtilisateurRepository;
import mg.airbnb.services.LogementService;
import mg.airbnb.services.RoleService;
import mg.airbnb.services.UtilisateurService;

@Controller
public class MainController {

	@Autowired
	private UtilisateurService utilisateurService;

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Autowired
	private BCryptPasswordEncoder bcrypt;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private RoleService roleService;

	@Autowired
	LogementService logementService;

	/**
	 * Retrieve login page
	 * 
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView loginPage() {
		ModelAndView model = new ModelAndView("loginPage");
		return model;
	}

	/**
	 * 
	 * 
	 * @return
	 */
	@RequestMapping(value = { "/home", "/" }, method = RequestMethod.GET)
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("homePage");
		if (utilisateurService.getLoggedUser() != null) {
			mav.addObject("utilisateur", utilisateurService.getLoggedUser());
			mav.addObject("notifications", logementService.countNotifications());
		}
		if (logementService.findAllAvailable(utilisateurService.getLoggedUser()) != null) {
			mav.addObject("logements", logementService.findAllAvailable(utilisateurService.getLoggedUser()));
		}

		return mav;
	}
}
