package mg.airbnb.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import mg.airbnb.model.Role;
import mg.airbnb.repository.RoleRepository;

@CrossOrigin(origins = "*")
@Controller
public class RoleController {

	@Autowired
	RoleRepository roleRepository;

	private final String url = "/role";

	@RequestMapping(value = url + "s", method = RequestMethod.GET)
	@ResponseBody
	public Set<Role> findAll() {
		return roleRepository.findAll();
	}
}
