package mg.airbnb.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import mg.airbnb.model.Location;
import mg.airbnb.model.Logement;
import mg.airbnb.model.TypeLogement;
import mg.airbnb.model.Utilisateur;
import mg.airbnb.repository.LocationRepository;
import mg.airbnb.repository.LogementRepository;
import mg.airbnb.services.LogementService;
import mg.airbnb.services.UtilisateurService;

@CrossOrigin(origins = "*")
@Controller
public class LogementController {

	@Autowired
	private LogementService logementService;

	@Autowired
	private UtilisateurService utilisateurService;

	@Autowired
	private LogementRepository logementRepository;

	@Autowired
	private LocationRepository locationRepository;

	private final String url = "/logement";

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = url + "s", method = RequestMethod.GET)
	public ModelAndView findAll() {
		ModelAndView mav = new ModelAndView("logements");
		Utilisateur utilisateur = utilisateurService.getLoggedUser();
		mav.addObject("logements", utilisateur.getLogements());
		mav.addObject("utilisateur", utilisateurService.getLoggedUser());
		mav.addObject("notifications", logementService.countNotifications());
		return mav;
	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = url + ".create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView mav = new ModelAndView("logement");
		mav.addObject("logement", new Logement());
		return mav;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = url + ".delete/{id}", method = RequestMethod.POST)
	public String delete(@PathVariable Long id) {
		Logement logement = logementService.findById(id);
		for (Location location : logement.getLocataires()) {
			locationRepository.delete(location);
		}
		logementRepository.deleteById(id);
		return "redirect:" + url + "s";
	}

	@RequestMapping(value = url
			+ ".store/{nbPieces}/{superficie}/{adresse}/{ville}/{prix}/{nbLocataire}/{type}/{dispo}", method = RequestMethod.POST)
	public String save(@PathVariable int nbPieces, @PathVariable int superficie, @PathVariable String adresse,
			@PathVariable String ville, @PathVariable Double prix, @PathVariable int nbLocataire,
			@PathVariable String type, @PathVariable Long dispo) {
		Date disponibilite = new Date(dispo);
		Logement logement = new Logement();
		logement.setNbPieces(nbPieces);
		logement.setSuperficie(superficie);
		logement.setAdresse(adresse);
		logement.setVille(ville);
		logement.setPrix(prix);
		logement.setNbLocataire(nbLocataire);
		logement.setType(type.equalsIgnoreCase("maison") ? TypeLogement.MAISON : TypeLogement.APPARTEMENT);
		logement.setDisponibilite(disponibilite);
		logement.setProprietaire(utilisateurService.getLoggedUser());
		logementRepository.save(logement);
		return "redirect:" + url + "s";
	}
}
