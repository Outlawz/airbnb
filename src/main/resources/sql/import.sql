TRUNCATE TABLE locations CASCADE;
TRUNCATE TABLE logements CASCADE;
TRUNCATE TABLE utilisateurs CASCADE;
TRUNCATE TABLE roles CASCADE;

-- AJOUT DES RÔLES
INSERT INTO roles (id, name) VALUES (1, 'Loueur');
INSERT INTO roles (id, name) VALUES (2, 'Locataire');

